package com.kotlin.notes.deadlock


class Account {
    var balance: Int = 0
    var name = ""

    constructor(balance: Int, name: String) {
        this.balance = balance
        this.name = name
    }

    fun deposit(amount: Int) {
        this.balance += amount

    }

    fun withdraw(amount: Int) {
        this.balance -= amount
    }

    override fun toString(): String {
        return "Account(balance=$balance, name='$name')"
    }

    companion object {
        fun transfer(from: Account, to: Account, amount: Int) {
            from.withdraw(amount)
            to.deposit(amount)
            println("${from} transfer ${to}: $amount")
        }
    }


}