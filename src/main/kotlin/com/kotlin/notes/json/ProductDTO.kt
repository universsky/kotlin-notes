package com.kotlin.notes.json

class ProductDTO {
    var id: String? = null
    var name: String? = null
    var children = mutableListOf<ProductDTO>()
}