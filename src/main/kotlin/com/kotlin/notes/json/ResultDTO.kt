package com.kotlin.notes.json

class ResultDTO<T> {
    var success = false
    var data: T? = null
}