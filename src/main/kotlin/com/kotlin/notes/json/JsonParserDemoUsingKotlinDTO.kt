package com.kotlin.notes.json

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONArray
import com.alibaba.fastjson.JSONObject
import com.alibaba.fastjson.TypeReference
import com.google.gson.Gson

val json = """
        {"data":[{"children":[{"children":[],"id":"2","name":"二级产品线 A1"},{"children":[],"id":"3","name":"二级产品线 A2"}],"id":"1","name":"一级产品线 A"},{"children":[],"id":"4","name":"一级产品线 B"},{"children":[],"id":"5","name":"一级产品线 C"}],"success":true}
    """.trimIndent()

class JsonParserDemoUsingKotlinDTO {

    class TypeRef : TypeReference<ResultDTO<List<ProductDTO>>>()


    fun getProductList1(): List<ProductDTO>? {
        return getResultDTO().data
    }

    fun getProductList2(): List<ProductDTO>? {
        return getResultDTOUsingTypeRef().data
    }


    fun getResultDTOUsingTypeRef(): ResultDTO<List<ProductDTO>> {
        val typeRef = TypeRef()
        val result = JSON.parseObject(json, typeRef)
        return result
    }

    fun getResultDTO(): ResultDTO<List<ProductDTO>> {

        val result = JSON.parseObject(json, ResultDTO::class.java)
        return result as ResultDTO<List<ProductDTO>>
    }

    fun getResultDTOUseGson(): ResultDTO<List<ProductDTO>> {
        val gson = Gson()
        val result = gson.fromJson(json, ResultDTO::class.java)
        val tResult =  result as ResultDTO<List<ProductDTO>>

        return tResult
    }


    fun getResultDTOUseGson2(): ResultDTO<List<ProductDTO>> {
        val gson = Gson()


        val result = gson.fromJson<ResultDTO<List<ProductDTO>>>(json, ResultDTO::class.java)
        return result
    }
}


fun main() {
    val demo = JsonParserDemoUsingKotlinDTO()

    demo.getResultDTOUseGson2()


    demo.getProductList1()
    demo.getProductList2()

    run {
        val result = demo.getResultDTO()
        val data = result.data
        println(data!!::class.java.canonicalName)
        println(data[0]!!::class.java.canonicalName)
    }

    run {
        val result = demo.getResultDTOUsingTypeRef()
        val data = result.data
        println(data!!::class.java.canonicalName)
        println(data[0]!!::class.java.canonicalName)
    }

    JSONArray()
    JSONObject()

}


fun generateJson(): String {
    val data = mutableListOf<ProductDTO>()

    run {
        val p = ProductDTO()
        p.id = "1"
        p.name = "一级产品线 A"

        val p2list = mutableListOf<ProductDTO>()

        run {
            val pp = ProductDTO()
            pp.id = "2"
            pp.name = "二级产品线 A1"
            p2list.add(pp)
        }
        run {
            val pp = ProductDTO()
            pp.id = "3"
            pp.name = "二级产品线 A2"
            p2list.add(pp)
        }

        p.children = p2list
        data.add(p)
    }
    run {
        val p = ProductDTO()
        p.id = "4"
        p.name = "一级产品线 B"
        data.add(p)
    }
    run {
        val p = ProductDTO()
        p.id = "5"
        p.name = "一级产品线 C"
        data.add(p)
    }


    val resultDTO = ResultDTO<List<ProductDTO>>()
    resultDTO.data = data
    resultDTO.success = true
    println(JSON.toJSONString(resultDTO))
    return JSON.toJSONString(resultDTO)
}