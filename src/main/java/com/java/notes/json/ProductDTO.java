package com.java.notes.json;

import java.util.List;

public class ProductDTO {
    String id;
    String name;
    List<ProductDTO> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ProductDTO> children) {
        this.children = children;
    }
}
